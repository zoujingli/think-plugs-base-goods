<?php

namespace plugin\base\goods\service;

use plugin\base\goods\model\ShopGoodsCate;
use plugin\base\goods\model\ShopGoodsItem;
use plugin\base\goods\model\ShopGoodsMark;
use think\admin\Library;
use think\admin\Service;

/**
 * 商品数据服务
 * Class GoodsService
 * @package plugin\base\goods\service
 */
class GoodsService extends Service
{
    /**
     * 更新商品库存数据
     * @param string $code
     */
    public static function stock(string $code)
    {
        Library::$sapp->event->trigger('SyncGoodsSync', $code);
    }

    /**
     * 商品数据绑定
     * @param array $data 商品主数据
     * @param boolean $simple 简化数据
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function bindData(array &$data = [], bool $simple = true): array
    {
        $marks = ShopGoodsMark::items();
        $cates = ShopGoodsCate::treeTable();
        $codes = array_unique(array_column($data, 'code'));
        $items = ShopGoodsItem::mk()->whereIn('goods_code', $codes)->where(['status' => 1])->select()->toArray();
        foreach ($data as &$vo) {
            [$vo['marks'], $vo['cateids'], $vo['cateinfo']] = [str2arr($vo['marks'], ',', $marks), str2arr($vo['cateids']), []];
            [$vo['slider'], $vo['specs'], $vo['items']] = [str2arr($vo['slider'], '|'), json_decode($vo['data_specs'], true), []];
            foreach ($cates as $cate) if (in_array($cate['id'], $vo['cateids'])) $vo['cateinfo'] = $cate;
            foreach ($items as $item) if ($item['goods_code'] === $vo['code']) $vo['items'][] = $item;
            if ($simple) unset($vo['marks'], $vo['sort'], $vo['status'], $vo['deleted'], $vo['data_items'], $vo['data_specs'], $vo['cateinfo']['parent']);
        }
        return $data;
    }
}