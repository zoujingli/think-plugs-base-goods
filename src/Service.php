<?php

// +----------------------------------------------------------------------
// | Goods Plugin for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2023 Anyon <zoujingli@qq.com>
// +----------------------------------------------------------------------
// | 官方网站: https://thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// | 免费声明 ( https://thinkadmin.top/disclaimer )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/think-plugs-base-goods
// +----------------------------------------------------------------------

namespace plugin\base\goods;

use think\admin\Plugin;

class Service extends Plugin
{
    protected $package = 'zoujingli/think-plugs-base-goods';

    public static function menu(): array
    {
        $name = app(static::class)->appName;
        return [
            [
                'name' => '商品管理',
                'subs' => [
                    ['name' => '商品数据管理', 'icon' => 'layui-icon layui-icon-star', 'node' => "{$name}/goods/index"],
                    ['name' => '商品分类管理', 'icon' => 'layui-icon layui-icon-tabs', 'node' => "{$name}/cate/index"],
                ]
            ]
        ];
    }
}