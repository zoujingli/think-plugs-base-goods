<?php

// +----------------------------------------------------------------------
// | Goods Plugin for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2023 Anyon <zoujingli@qq.com>
// +----------------------------------------------------------------------
// | 官方网站: https://thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// | 免费声明 ( https://thinkadmin.top/disclaimer )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/think-plugs-base-goods
// +----------------------------------------------------------------------

namespace plugin\base\goods\controller;

use plugin\base\goods\model\ShopGoodsMark;
use think\admin\Controller;
use think\admin\helper\QueryHelper;

/**
 * 商品标签管理
 * Class Mark
 * @package plugin\base\goods\controller\shop
 */
class Mark extends Controller
{
    /**
     * 商品标签管理
     * @auth true
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        ShopGoodsMark::mQuery()->layTable(function () {
            $this->title = '商品标签管理';
        }, function (QueryHelper $query) {
            $query->like('name')->equal('status')->dateBetween('create_at');
        });
    }

    /**
     * 添加商品标签
     * @auth true
     */
    public function add()
    {
        ShopGoodsMark::mForm('form');
    }

    /**
     * 编辑商品标签
     * @auth true
     */
    public function edit()
    {
        ShopGoodsMark::mForm('form');
    }

    /**
     * 修改商品标签状态
     * @auth true
     */
    public function state()
    {
        ShopGoodsMark::mSave();
    }

    /**
     * 删除商品标签
     * @auth true
     */
    public function remove()
    {
        ShopGoodsMark::mDelete();
    }
}