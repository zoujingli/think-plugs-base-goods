<?php

// +----------------------------------------------------------------------
// | Goods Plugin for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2023 Anyon <zoujingli@qq.com>
// +----------------------------------------------------------------------
// | 官方网站: https://thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// | 免费声明 ( https://thinkadmin.top/disclaimer )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/think-plugs-base-goods
// +----------------------------------------------------------------------

namespace plugin\base\goods\model;

use think\admin\Model;

/**
 * 商城商品库存模型
 * Class ShopGoodsStock
 * @package plugin\base\goods\model
 */
class ShopGoodsStock extends Model
{

}