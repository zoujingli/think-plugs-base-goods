# ThinkPlugsBaseGoods for ThinkAdmin

[![Latest Stable Version](https://poser.pugx.org/zoujingli/think-plugs-base-goods/v/stable)](https://packagist.org/packages/zoujingli/think-plugs-base-goods)
[![Latest Unstable Version](https://poser.pugx.org/zoujingli/think-plugs-base-goods/v/unstable)](https://packagist.org/packages/zoujingli/think-plugs-base-goods)
[![Total Downloads](https://poser.pugx.org/zoujingli/think-plugs-base-goods/downloads)](https://packagist.org/packages/zoujingli/think-plugs-base-goods)
[![Monthly Downloads](http://img.shields.io/packagist/dm/zoujingli/think-plugs-base-goods.svg)](https://packagist.org/packages/zoujingli/think-plugs-base-goods)
[![License](https://poser.pugx.org/zoujingli/think-plugs-base-goods/license)](https://packagist.org/packages/zoujingli/think-plugs-base-goods)

**ThinkAdmin** 的基础商品管理插件，目前处于开发中，不可直接使用。

### 安装插件

```shell
# 注意，插件仅支持在 ThinkAdmin v6 系统中使用
composer require zoujingli/think-plugs-base-goods
```

### 卸载插件

```shell
# 卸载后通过 composer update 时不会再更新，其他依赖除外
composer remove zoujingli/think-plugs-base-goods
```
